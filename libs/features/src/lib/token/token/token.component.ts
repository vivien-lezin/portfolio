import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FadeInFromLeftAnimationDirective} from "@portfolio/animation-angular";
import {SkillType} from "@portfolio/portfolio-type";

@Component({
  selector: 'ptf-token',
  standalone: true,
  imports: [CommonModule, FadeInFromLeftAnimationDirective],
  templateUrl: './token.component.html',
  styleUrl: './token.component.scss',
})
export class TokenComponent {
  @Input({required: true}) public token!: SkillType.Skill;
  @Input({required: true}) public count!: number;
  @Input({required: true}) public index!: number;
}
