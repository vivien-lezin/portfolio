export const PORT = 4000;

export const URL = 'http://localhost:';

export default () => ({
  port: process.env.PORT ? parseInt(process.env.PORT) : PORT,
});
