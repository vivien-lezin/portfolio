import {CoreAnimationDirective} from "./core-animation-directive";
import {AfterViewInit, Directive, ElementRef} from "@angular/core";

@Directive({
  selector: '[ptfFromBottomInAnimation]',
  standalone: true,
})
export class FromBottomInAnimationDirective extends CoreAnimationDirective implements AfterViewInit{
  constructor(protected override element: ElementRef) {
    super(element);
  }
  public override ngAfterViewInit(): void {
    super.ngAfterViewInit()
    // perform animation
    this._animateIn();
  }
  protected override _animateIn() {
    this._timeline.from(this.element.nativeElement, {
      duration: this.duration,
      top: "100dvh",
      delay: this.delay
    })
    super._animateIn();
  }
}
