import {Component, EventEmitter, Input, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatIcon} from "@angular/material/icon";

@Component({
  selector: 'ptf-icon-desktop',
  standalone: true,
  imports: [CommonModule, MatIcon],
  templateUrl: './icon-desktop.component.html',
  styleUrl: './icon-desktop.component.scss',
})
export class IconDesktopComponent {

  @Input() iconSrc!: string;
  @Input() iconText!: string;
  @Output() clicked = new EventEmitter<string>();


}
