import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButton} from "@angular/material/button";
import {MatIcon} from "@angular/material/icon";
import {FadeInAnimationDirective, FadeInFromRightAnimationDirective} from "@portfolio/animation-angular";
import {BigTitleComponent} from "@portfolio/features";
import {LayoutService} from "@portfolio/layout";
import {HttpClient} from "@angular/common/http";
import {lastValueFrom} from "rxjs";
import * as mammoth from "mammoth";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'ptf-home',
  standalone: true,
  imports: [CommonModule, MatButton, MatIcon, FadeInFromRightAnimationDirective, BigTitleComponent, BigTitleComponent, FadeInAnimationDirective],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent implements OnInit, OnDestroy {
  private _contentArticle!: string;

  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _contentArticleService: LayoutService.ContentArticleService,
    private readonly _titleService: Title,
    private readonly _metaService: Meta) {
  }

  public async ngOnInit(): Promise<void> {
    this._titleService.setTitle("Présentation | Vivien LEZIN - Développeur web fullstack");
    this._metaService.updateTag({
      name: 'description',
      content: 'Développeur web fullstack Angular, Node.js, Rust et surtout créatif. Découvrez mon parcours, mes valeurs et mes compétences.'
    });
    const content = lastValueFrom(this._httpClient.get("./assets/documents/Présentation générale.docx", {responseType: 'arraybuffer'}));
    this._contentArticle = (await mammoth.convertToHtml({arrayBuffer: await content})).value;
  }

  public ngOnDestroy() {
    this._contentArticleService.removeContent();
  }

  public onClickPresentation(): void {
    this._contentArticleService.setContent(this._contentArticle, "Présentation");
  }
}
