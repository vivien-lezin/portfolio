import {Component, HostListener, Input, OnChanges, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ptf-menu-burger',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './menu-burger.component.html',
  styleUrl: './menu-burger.component.scss',
})
export class MenuBurgerComponent implements OnChanges {
  private _className = "is-closed";
  @Input()
  public closed = false;

  public get className() {
    return this._className;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes['closed'].currentValue) {
      this._className = 'is-closed';
    } else {
      this._className = 'is-open';
    }
  }
}
