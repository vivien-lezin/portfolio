export type Skill = {
  name: string;
  mainImg?: string;
  content: string;
  isHard: boolean;
  tokenImg: string;
  slug: string;
  note: 1 | 2 | 3 | 4 | 5;
  filePath: string;
};
