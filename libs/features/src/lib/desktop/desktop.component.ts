import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FadeInFromLeftAnimationDirective} from "@portfolio/animation-angular";
import {IconDesktopComponent} from "./icon-desktop/icon-desktop.component";
import {PopupDesktopComponent} from "./popup-desktop/popup-desktop.component";
import {CreationType} from "@portfolio/portfolio-type";

@Component({
  selector: 'ptf-desktop',
  standalone: true,
  imports: [CommonModule, FadeInFromLeftAnimationDirective, IconDesktopComponent, PopupDesktopComponent],
  templateUrl: './desktop.component.html',
  styleUrl: './desktop.component.scss',
})
export class DesktopComponent {
  @Input() public creations!: CreationType.CreationToDisplay[];

  clicked(title: string) {
    const creation = this.creations.find((c)=>c.title === title);
    if (creation) {
      for (const c of this.creations) {
        if (c.title === title){
          c.display = true;
          c.zIndex = 1025;
        } else {
          c.zIndex = c.zIndex > 1021 ? c.zIndex - 1 : 1021;
        }
      }
    }
  }

  closed(title: string) {
    const creation = this.creations.find((c)=>c.title === title);
    if (creation) {
      creation.mainFocus = false;
      creation.display = false;
    }
  }

  windowGrown(title: string) {
    const creation = this.creations.find((c)=>c.title === title);
    if (creation) {
      creation.mainFocus = !creation.mainFocus;
    }
  }
}
