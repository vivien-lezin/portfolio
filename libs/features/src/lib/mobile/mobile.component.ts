import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CreationType} from "@portfolio/portfolio-type";
import {FadeInFromLeftAnimationDirective} from "@portfolio/animation-angular";
import {IconDesktopComponent} from "../desktop/icon-desktop/icon-desktop.component";
import {IconMobileComponent} from "./icon-mobile/icon-mobile.component";
import {AppMobileComponent} from "./app-mobile/app-mobile.component";

@Component({
  selector: 'ptf-mobile',
  standalone: true,
  imports: [CommonModule, FadeInFromLeftAnimationDirective, IconDesktopComponent, IconMobileComponent, AppMobileComponent],
  templateUrl: './mobile.component.html',
  styleUrl: './mobile.component.scss',
})
export class MobileComponent {
  @Input() public creations!: CreationType.CreationToDisplay[];

  public clicked(title: string) {
    const creation = this.creations.find((c)=>c.title === title);
    if (creation) {
      for (const c of this.creations) {
        if (c.title === title){
          c.display = true;
          c.zIndex = 1031;
        } else {
          c.zIndex = c.zIndex > 1027 ? c.zIndex - 1 : 1027;
        }
      }
    }
  }

  closed(title: string) {
    const creation = this.creations.find((c)=>c.title === title);
    if (creation) {
      creation.mainFocus = false;
      creation.display = false;
    }
  }

}
