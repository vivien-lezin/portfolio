import {Component, EventEmitter, Input, Output} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TokenComponent} from "./token/token.component";
import {
  FadeInFromBottomAnimationDirective
} from "@portfolio/animation-angular";
import {SkillType} from "@portfolio/portfolio-type";

@Component({
  selector: 'ptf-token-group',
  standalone: true,
  imports: [CommonModule, TokenComponent, FadeInFromBottomAnimationDirective],
  templateUrl: './token-group.component.html',
  styleUrl: './token-group.component.scss',
})
export class TokenGroupComponent {
  @Input({required: true}) public skills!: SkillType.SkillToDisplay[];
  @Output() public tokenSelected: EventEmitter<SkillType.SkillToDisplay> = new EventEmitter<SkillType.SkillToDisplay>();
  public label?: string;
}
