import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ContentArticleService} from "../service";
import {
  FromBottomInAnimationDirective,
  HeightOutAnimationDirective,
  NgIfAnimatedDirective
} from "@portfolio/animation-angular";
import {MatIcon} from "@angular/material/icon";
import {MatMiniFabButton} from "@angular/material/button";

@Component({
  selector: 'ptf-article',
  standalone: true,
  imports: [CommonModule, NgIfAnimatedDirective, FromBottomInAnimationDirective, MatIcon, MatMiniFabButton, HeightOutAnimationDirective],
  templateUrl: './article.component.html',
  styleUrl: './article.component.scss',
})
export class ArticleComponent {

  constructor(
    private _contentArticleService: ContentArticleService
  ) {}

  public get imageUrl(): string | undefined {
    return this._contentArticleService.imageUrl;
  }

  public get display(): boolean {
    return this._contentArticleService.display;
  }

  public get title(): string | undefined {
    return this._contentArticleService.title;
  }

  public get content(): string | undefined {
    return this._contentArticleService.content;
  }

  public onClickClose(): void {
    this._contentArticleService.onDismissArticle();
    this._contentArticleService.removeContent();
  }
}
