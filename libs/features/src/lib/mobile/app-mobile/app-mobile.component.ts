import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  FadeInFromTinyAnimationDirective,
  FadeOutAnimationDirective,
  NgIfAnimatedDirective
} from "@portfolio/animation-angular";
import {MatIcon} from "@angular/material/icon";
import {MatProgressSpinner} from "@angular/material/progress-spinner";

@Component({
  selector: 'ptf-app-mobile',
  standalone: true,
  imports: [CommonModule, FadeInFromTinyAnimationDirective, FadeOutAnimationDirective, MatIcon, MatProgressSpinner, NgIfAnimatedDirective],
  templateUrl: './app-mobile.component.html',
  styleUrl: './app-mobile.component.scss',
})
export class AppMobileComponent implements OnDestroy, OnChanges {

  @Input({required: true}) public iconImg!: string;
  @Input({required: true}) public title!: string;
  @Input({required: true}) public content!: string;
  @Input({required: true}) public display!: boolean;

  @Input() public mainImg?: string;
  @Output("closed") private _closed = new EventEmitter<string>();

  private _timeoutHandler: number[] = [];

  private _loaderEnd = false;

  public get loaderEnd() {
    return this._loaderEnd;
  }

  private _launchTimeout() {
    this._timeoutHandler.push(setTimeout(() => {
      this._loaderEnd = true;
    }, 1500));
  }

  private _clearTimeout(){
    this._timeoutHandler.forEach((t) => clearTimeout(t));
  }

  public ngOnDestroy(): void {
    this._clearTimeout();
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes['display']) {
      const newValue: boolean = changes['display'].currentValue;
      if (newValue) {
        this._launchTimeout();
      } else {
        this._clearTimeout();
      }
    }
  }

  public closed(title: string): void {
    this._loaderEnd = false;
    this._closed.emit(title);
  }

}
