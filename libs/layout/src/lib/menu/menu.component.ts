import {AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatIcon} from "@angular/material/icon";
import {
  FadeInFromLeftAnimationDirective,
  FadeInFromRightAnimationDirective, HeightOutAnimationDirective,
  NgIfAnimatedDirective
} from "@portfolio/animation-angular";
import {LinkMenuComponent} from "./link-menu/link-menu.component";
import {gsap} from "gsap";
import {Link} from "@portfolio/features";
import {DeviceService} from "../service";

type LinkIcon = {
  link: string;
  default: string;
  hover: string;
  hovered: boolean;
}

@Component({
  selector: 'ptf-menu',
  standalone: true,
  imports: [CommonModule, MatIcon, FadeInFromLeftAnimationDirective, NgIfAnimatedDirective, LinkMenuComponent, FadeInFromRightAnimationDirective, HeightOutAnimationDirective],
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.scss',
})
export class MenuComponent implements AfterViewInit {
  @Output() closed: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() links!: Array<Link>;
  public closeSubMenu1 = false;
  public closeSubMenu2 = false;
  public closeSubMenu3 = false;
  @ViewChild("menuDeviderDesktop") private _menuDeviderDesktopElt!: ElementRef<HTMLDivElement>;
  @ViewChild("menuDeviderMobile") private _menuDeviderMobileElt!: ElementRef<HTMLDivElement>;
  private _timelineMenuDevider = gsap.timeline({
    paused: true,
    reversed: true
  });

  public get menuExpanded(): boolean {
    return (this.closeSubMenu1 || this.closeSubMenu2 || this.closeSubMenu3) && this._deviceService.isXSmallScreen;
  }

  public readonly linkIcons: LinkIcon[] = [{
    default: "./assets/images/linkedin.svg",
    hover: "./assets/images/linkedin-hover.svg",
    link: "https://www.linkedin.com/in/vivien-lezin-graphiste-et-developpeur-web/",
    hovered : false
  }, {
    default: "./assets/images/codingame.svg",
    hover: "./assets/images/codingame-hover.svg",
    link: "https://www.codingame.com/profile/241c73b3e82319f6140ca0a17a1222cf7146334",
    hovered : false
  }, {
    default: "./assets/images/gitlab.svg",
    hover: "./assets/images/gitlab-hover.svg",
    link: "https://gitlab.com/vivien-lezin",
    hovered : false
  }]

  constructor(private _deviceService: DeviceService) {
  }

  public getCloseMenu(index: number) {
    switch (index) {
      case 1:
        return this.closeSubMenu1;
      case 2:
        return this.closeSubMenu2;
      case 3:
        return this.closeSubMenu3;
      default:
        throw new Error("Index not handled " + index);
    }
  }

  public ngAfterViewInit(): void {
    if (this._menuDeviderDesktopElt !== undefined) {
      if (this._deviceService.isDesktop) {
        this._timelineMenuDevider.to(this._menuDeviderDesktopElt.nativeElement, {
          height: "100%",
          duration: 0.3,
          delay: 0,
        });
        this._timelineMenuDevider.play();
      } else {
        this._timelineMenuDevider.from(this._menuDeviderMobileElt.nativeElement, {
          width: "0",
          duration: 0.3,
          delay: 0,
        });
        this._timelineMenuDevider.play();
      }
    }
  }

  public onExpandMainLink(value: boolean, index: number) {
    if (value) {
      this.closeSubMenu1 = false;
      this.closeSubMenu2 = false;
      this.closeSubMenu3 = false;
      return;
    }
    switch (index) {
      case 1:
        this.closeSubMenu1 = !value;
        this.closeSubMenu2 = value;
        this.closeSubMenu3 = value;
        break;
      case 2:
        this.closeSubMenu1 = value;
        this.closeSubMenu2 = !value;
        this.closeSubMenu3 = value;
        break;
      case 3:
        this.closeSubMenu1 = value;
        this.closeSubMenu2 = value;
        this.closeSubMenu3 = !value;
        break;
    }
  }

  public onIconOvered(linkIcon?: LinkIcon) {
    this.linkIcons.forEach((l) => l.hovered = false);
    if (linkIcon !== undefined) {
      linkIcon.hovered = true;
    }
  }

  public get iconOvered(): boolean {
    return !!this.linkIcons.find((l)=>l.hovered)
  }
}
