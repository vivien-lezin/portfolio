import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BigTitleComponent} from "@portfolio/features";
import {LayoutService} from "@portfolio/layout";
import {FadeInFromLeftAnimationDirective, FadeInFromTinyAnimationDirective} from "@portfolio/animation-angular";
import {MatAnchor} from "@angular/material/button";
import {Meta, Title} from "@angular/platform-browser";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'ptf-error-404',
  standalone: true,
  imports: [CommonModule, BigTitleComponent, FadeInFromLeftAnimationDirective, FadeInFromTinyAnimationDirective, MatAnchor, RouterLink],
  templateUrl: './error-404.component.html',
  styleUrl: './error-404.component.scss',
})
export class Error404Component implements OnInit, AfterViewInit, OnDestroy {
  constructor(
    private readonly _deviceService: LayoutService.DeviceService,
    private readonly _titleService: Title,
    private readonly _metaService: Meta) {
  }

  public ngOnDestroy(): void {
    this._metaService.removeTag('name=\'robots\'');
  }

  public ngOnInit(): void {
    this._titleService.setTitle("404 | Vivien LEZIN - Développeur web fullstack");
    this._metaService.updateTag({
      name: 'description',
      content: 'Something wrong.'
    });
    this._metaService.addTag({name: 'robots', content: 'noindex, nofollow'});
  }

  public ngAfterViewInit(): void {
    this.initializeGame();
  }

  public initializeGame(): void {
    const canvas = document.getElementById('gameCanvas') as HTMLCanvasElement;
    if (this._deviceService.isLargeScreen) {
      canvas.width = 800;
    } else if (this._deviceService.isMediumScreen) {
      canvas.width = 600;
    } else if (this._deviceService.isSmallScreen) {
      canvas.width = 400;
    } else {
      return;
    }
    const ctx = canvas.getContext('2d')!;
    const MIN_DISTANCE = 150; // Distance minimale entre obstacles
    const INITIAL_GAME_SPEED = 3;
    canvas.height = 200;

    // Charger les images
    const dinoImages = [
      new Image(),
      new Image(),
    ];
    dinoImages[0].src = 'assets/images/dino1.png';
    dinoImages[1].src = 'assets/images/dino2.png';

    const cactusImg1 = new Image();
    cactusImg1.src = 'assets/images/cactus1.png'; // Image du cactus
    const cactusImg2 = new Image();
    cactusImg2.src = 'assets/images/cactus2.png'; // Image du cactus
    const cactusImg3 = new Image();
    cactusImg3.src = 'assets/images/cactus3.png'; // Image du cactus

    const backgroundImg1 = new Image();
    backgroundImg1.src = 'assets/images/background_dino1.png';
    const backgroundImg2 = new Image();
    backgroundImg2.src = 'assets/images/background_dino2.png';
    const backgroundImg3 = new Image();
    backgroundImg3.src = 'assets/images/background_dino3.png';

    const birdImages = [
      new Image(),
      new Image(),
    ];
    birdImages[0].src = 'assets/images/bird1.png';
    birdImages[1].src = 'assets/images/bird2.png';


    const errorDelta = 5;


    // Variables du jeu
    let dino = {x: 50, y: 150, width: 40, height: 40, dy: 0, gravity: 1};
    let obstacles: { x: number; y: number; width: number; height: number, img: HTMLImageElement }[] = [];
    let isJumping = false;
    let score = 0;
    let gameSpeed = INITIAL_GAME_SPEED;
    let topScore = Number(localStorage.getItem('topScore')) || 0;
    let frame = 0; // Pour animer le dino
    let birds: { x: number; y: number; width: number; height: number }[] = [];
    let birdFrame = 0; // Frame actuelle pour l'animation

    // **Variables pour le décor défilant**
    let bgX1 = 0; // Position de départ du fond


    // **Variables pour le décor défilant**
    let bgX2 = 0; // Position de départ du fond


    // **Variables pour le décor défilant**
    let bgX3 = 0; // Position de départ du fond

    let isOver = false;
    const SpaceHandler = (e: KeyboardEvent) => {
      if (e.code === 'Space') {
        if (!isJumping) {
          dino.dy = -15;
          isJumping = true;
        }
      }
    }
    // Gestion des touches
    document.addEventListener('keydown', SpaceHandler);

    function restartGame(e: KeyboardEvent) {
      if (e.code === 'Space') {
        // Réinitialiser toutes les variables
        dino = {x: 50, y: 150, width: 40, height: 40, dy: 0, gravity: 1};
        obstacles = [];
        birds = [];
        gameSpeed = INITIAL_GAME_SPEED;
        isJumping = false;
        score = 0;
        isOver = false;
        frame = 0;

        // Supprimer l'écouteur d'événement pour éviter des conflits
        document.removeEventListener('keydown', restartGame);

        // Recommencer la boucle
        gameLoop();
      }
    }

    // Boucle du jeu
    function gameLoop() {
      const bgSpeed1 = gameSpeed / 2; // Vitesse du défilement
      const bgSpeed2 = gameSpeed; // Vitesse du défilement
      const bgSpeed3 = gameSpeed / 10; // Vitesse du défilement
      // Réduire la distance minimale en fonction du score
      const dynamicMinDistance = MIN_DISTANCE + Math.floor(gameSpeed / INITIAL_GAME_SPEED) * 10;

      // Augmenter la vitesse du jeu
      gameSpeed = INITIAL_GAME_SPEED + Math.floor(score / 10) / 2;
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      // **Dessiner le fond défilant**
      bgX1 -= bgSpeed1;
      if (bgX1 <= -canvas.width) {
        bgX1 = 0; // Répéter le fond
      }
      ctx.drawImage(backgroundImg1, bgX1, 0, canvas.width, canvas.height);
      ctx.drawImage(backgroundImg1, bgX1 + canvas.width, 0, canvas.width, canvas.height);

      // **Dessiner le fond défilant**
      bgX2 -= bgSpeed2;
      if (bgX2 <= -canvas.width) {
        bgX2 = 0; // Répéter le fond
      }
      ctx.drawImage(backgroundImg2, bgX2, 0, canvas.width, canvas.height);
      ctx.drawImage(backgroundImg2, bgX2 + canvas.width, 0, canvas.width, canvas.height);

      // **Dessiner le fond défilant**
      bgX3 -= bgSpeed3;
      if (bgX3 <= -canvas.width) {
        bgX3 = 0; // Répéter le fond
      }
      ctx.drawImage(backgroundImg3, bgX3, 0, canvas.width, canvas.height);
      ctx.drawImage(backgroundImg3, bgX3 + canvas.width, 0, canvas.width, canvas.height);

      // **Animation du dinosaure**
      const dinoFrame = Math.floor(frame / 10) % 2; // Alterne entre 0 et 1
      ctx.drawImage(dinoImages[dinoFrame], dino.x, dino.y, dino.width, dino.height);

      // Appliquer la gravité
      dino.dy += dino.gravity;
      dino.y += dino.dy;

      // Empêcher de descendre sous le sol
      if (dino.y > 150) {
        dino.y = 150;
        dino.dy = 0;
        isJumping = false;
      }

      // Générer des obstacles
      if (Math.random() < 0.01) {
        if ((obstacles.length === 0 || obstacles[obstacles.length - 1].x < canvas.width - dynamicMinDistance) &&
          (birds.length === 0 || birds[birds.length - 1].x < canvas.width - dynamicMinDistance)) {
          let cactusImg;
          // Afficher l'image du cactus
          if (Math.random() < 0.33) {
            cactusImg = cactusImg1;
          } else if (Math.random() > 0.66) {
            cactusImg = cactusImg2;
          } else {
            cactusImg = cactusImg3;
          }
          obstacles.push({x: 800, y: 160, width: 20, height: 35, img: cactusImg});
        }
      }

      // Générer des oiseaux aléatoirement
      if (Math.random() < 0.01 / 2) { // Probabilité plus faible que les cactus
        if (
          (obstacles.length === 0 || obstacles[obstacles.length - 1].x < canvas.width - dynamicMinDistance) &&
          (birds.length === 0 || birds[birds.length - 1].x < canvas.width - dynamicMinDistance)
        ) {
          birds.push({x: 800, y: Math.random() < 0.5 ? 100 : 130, width: 30, height: 25}); // Deux hauteurs possibles
        }
      }

      // Dessiner les obstacles
      for (let i = 0; i < obstacles.length; i++) {
        const obs = obstacles[i];
        obs.x -= gameSpeed;


        ctx.drawImage(obs.img, obs.x, obs.y, obs.width, obs.height);

        // Détection des collisions
        if (
          dino.x < obs.x + obs.width - errorDelta &&
          dino.x + dino.width - errorDelta > obs.x &&
          dino.y < obs.y + obs.height - errorDelta &&
          dino.y + dino.height - errorDelta > obs.y
        ) {
          isOver = true;
        }

        // Supprimer les obstacles hors de l'écran
        if (obs.x + obs.width < 0) {
          obstacles.splice(i, 1);
          score++;
        }
      }

      // Dessiner et déplacer les oiseaux
      if (!isOver) {
        for (let i = 0; i < birds.length; i++) {
          const bird = birds[i];
          bird.x -= gameSpeed; // Faire avancer l'oiseau vers la gauche

          // Alterner les images toutes les 10 frames
          const birdSprite = birdImages[Math.floor(birdFrame / 15) % 2]; // Alterne entre 0 et 1

          // Dessiner l'oiseau avec l'image animée
          ctx.drawImage(birdSprite, bird.x, bird.y, bird.width, bird.height);

          // Détection des collisions
          if (
            dino.x < bird.x + bird.width - errorDelta &&
            dino.x + dino.width - errorDelta > bird.x &&
            dino.y < bird.y + bird.height - errorDelta &&
            dino.y + dino.height - errorDelta > bird.y
          ) {
            isOver = true;
          }

          // Supprimer les oiseaux hors de l’écran
          if (bird.x + bird.width < 0) {
            birds.splice(i, 1);
            score++; // Ajouter au score lorsqu'un oiseau est évité
          }
        }
      }


      // Afficher le score
      ctx.fillStyle = '#454F2C';
      ctx.font = '12px Retro Gaming';
      ctx.fillText('Hi: ' + topScore, 10, 20);
      ctx.font = '20px Retro Gaming';
      ctx.fillText('Score: ' + score, 10, 45);
      // Afficher le Top Score
      if (!isOver) {
        frame++;
        birdFrame++;
        requestAnimationFrame(gameLoop);
      } else {

        if (score > topScore) {
          topScore = score; // Mettre à jour le top score
          localStorage.setItem('topScore', String(topScore)); // Enregistrer dans le LocalStorage
        }

        ctx.font = '40px Retro Gaming';
        //260
        ctx.fillText('Game Over', (260 - canvas.width)/-2, 50);
        ctx.font = '30px Retro Gaming';
        //200
        ctx.fillText('Continue?', (200 - canvas.width)/-2, 100);

        // Écouter la touche Espace pour redémarrer
        document.addEventListener('keydown', restartGame);
        return; // Arrêter la boucle du jeu
      }
    }

    gameLoop();
  }

}
