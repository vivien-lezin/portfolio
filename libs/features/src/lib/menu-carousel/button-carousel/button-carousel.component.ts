import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ptf-button-carousel',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './button-carousel.component.html',
  styleUrl: './button-carousel.component.scss',
})
export class ButtonCarouselComponent {
  @Input({required: true}) public enabled!:boolean;
}
