import {AfterViewInit, Directive, ElementRef, EventEmitter, Input, Output, ViewContainerRef} from "@angular/core";
import * as gsap from "gsap";
@Directive()
export class CoreAnimationDirective implements AfterViewInit{
  @Input() duration = 1;
  @Input() delay = 0;

  @Output() completed: EventEmitter<null> = new EventEmitter();
  @Output() reverseCompleted: EventEmitter<null> = new EventEmitter();
  protected _timeline = gsap.gsap.timeline({
    onComplete: _ => this.completed.emit(),
    onReverseComplete: _ => this.reverseCompleted.emit(),
    paused:true,
    reversed:true
  });

  constructor(protected element: ElementRef) {

  }

  public ngAfterViewInit(): void {
    this.element.nativeElement.addEventListener('animate-out', (event:any) => {
      this.animateOut(event.detail.parentViewRef);
    })
  }

  protected _animateIn(): void {
    if(this._timeline.isActive()) {
      this._timeline.kill();
    }
    this._timeline.play();
  }

  protected animateOut(parentViewRef: ViewContainerRef) {
    if(this._timeline.isActive()) {
      this._timeline.kill();
    }
    setTimeout(() => {
      this._timeline.reverse();
      setTimeout(() => {
        if (parentViewRef) {
          parentViewRef.clear();
        }
      }, this.duration * 1000);
    }, 0);
  }
}
