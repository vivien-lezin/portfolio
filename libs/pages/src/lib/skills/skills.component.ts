import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BigTitleComponent, TokenGroupComponent, VerticalButtonComponent} from "@portfolio/features";
import {FadeInAnimationDirective} from "@portfolio/animation-angular";
import {SkillsService} from "./service";
import {SkillType} from "@portfolio/portfolio-type";
import {LayoutService} from "@portfolio/layout";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom, Subject, takeUntil} from "rxjs";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'ptf-skills',
  standalone: true,
  imports: [CommonModule, BigTitleComponent, FadeInAnimationDirective, TokenGroupComponent, VerticalButtonComponent],
  templateUrl: './skills.component.html',
  styleUrl: './skills.component.scss',
})
export class SkillsComponent implements OnInit, OnDestroy {

  private _onDestroy = new Subject<void>();

  constructor(private readonly _titleService: Title,
              private readonly _metaService: Meta,
              private readonly _skillsService: SkillsService,
              private readonly _contentArticleService: LayoutService.ContentArticleService,
              private _route: ActivatedRoute,
              private _router: Router) {
  }

  public get skills(): SkillType.SkillToDisplay[] {
    return this._skillsService.skillsToDisplay;
  }

  public get isHard(): boolean {
    return this._skillsService.isHard
  }

  public ngOnInit(): void {
    this._titleService.setTitle("Compétences | Vivien LEZIN - Développeur web fullstack");
    this._metaService.updateTag({
      name: 'description',
      content: 'Explorez mes compétences techniques et humaines : développement web Angular/Node.js/Rust, design UX/UI, gestion de projets et soft skill liès à l\'ingénierie logicielle.'
    });
    this._contentArticleService.onDismiss.pipe(takeUntil(this._onDestroy)).subscribe(() => {
      this._router.navigate(['compétences']);
    });
    this._route.paramMap.pipe(takeUntil(this._onDestroy)).subscribe(async (params) => {
      const selectedId = params.get('id');
      if (!this._skillsService.ready) {
        await lastValueFrom(this._skillsService.ready$)
      }
      if (selectedId) {
        const skillFound = this._skillsService.allSkills.find((skill) => skill.slug === selectedId);
        if (skillFound) {
          this._contentArticleService.setContent(skillFound.content, skillFound.name, skillFound.tokenImg);
          this.onclickButton(skillFound.isHard);
        } else {
          this._router.navigate(['404']);
        }
      } else {
        this._contentArticleService.removeContent();
      }
    });
  }

  public onclickButton(isHard: boolean) {
    if (this._skillsService.isHard !== isHard){
      this._skillsService.isHard = isHard;
    }
  }

  public onTokenSelected(skill: SkillType.Skill) {
    this._router.navigate(['compétences/' + skill.slug]);
  }

  public ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this._contentArticleService.removeContent();
  }
}
