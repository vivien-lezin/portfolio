import {
  CreationsComponent,
  Error404Component,
  ExperiencesComponent,
  HomeComponent,
  SkillsComponent
} from "@portfolio/pages";
import {Link} from "@portfolio/features";

export const appRoutes: Link[] = [
  {
    path: '',
    enabled: false,
    label: "Présentation",
    component: HomeComponent
  },
  {
    path: 'compétences',
    enabled: false,
    label: "Compétences",
    component: SkillsComponent,
    subLink: []
  },
  {
    path: 'compétences/:id',
    enabled: false,
    label: "Compétences",
    component: SkillsComponent,
  },
  {
    path: 'expériences',
    enabled: false,
    label: "Expériences",
    component: ExperiencesComponent,
  },
  {
    path: 'réalisations',
    enabled: false,
    label: "Réalisations",
    component: CreationsComponent
  },
  {
    path: 'réalisations/:id',
    enabled: false,
    label: "Réalisations",
    component: CreationsComponent
  },
  {
    path: '**',
    enabled: false,
    label: "Error404",
    component: Error404Component
  }
];
