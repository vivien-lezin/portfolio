import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  private _userAgent = navigator.userAgent.toLowerCase();
  private _isTouchScreen = 'ontouchstart' in window || navigator.maxTouchPoints > 0;

  public get isMobile(): boolean {
    return /android|webos|iphone|ipod|blackberry|iemobile|opera mini/.test(this._userAgent) && this._isTouchScreen;
  }

  public get isTablet(): boolean {
    return /ipad|tablet|playbook|silk/.test(this._userAgent) || (this.isMobile && window.innerWidth > 575);
  }

  public get isDesktop(): boolean {
    return !this.isMobile && !this.isTablet;
  }

  public get isXSmallScreen(): boolean {
    return window.innerWidth <= 575
  }

  public get isSmallScreen(): boolean {
    return  575 < window.innerWidth && window.innerWidth <= 768
  }

  public get isMediumScreen(): boolean {
    return 768 < window.innerWidth && window.innerWidth <= 1200;
  }

  public get isLargeScreen(): boolean {
    return window.innerWidth > 1200
  }
}
