import {Injectable} from '@angular/core';
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ContentArticleService {

  private _timeoutHandler?: number;

  private _content?: string;

  public get content(): string | undefined {
    return this._content;
  }

  private _title?: string;

  get title(): string | undefined {
    return this._title;
  }

  private _imageUrl?: string;

  public get imageUrl(): string | undefined {
    return this._imageUrl;
  }

  private _display = false;

  public get display(): boolean {
    return this._display;
  }

  private _onDismiss: Subject<void> = new Subject<void>();

  public get onDismiss(): Observable<void> {
    return this._onDismiss.asObservable();
  }

  public setContent(content: string, title: string, imgUrl?: string): void {
    clearTimeout(this._timeoutHandler);
    this._display = true;
    this._content = content;
    this._title = title;
    this._imageUrl = imgUrl;
  }

  public onDismissArticle(): void {
    this._onDismiss.next();
  }

  public removeContent(): void {
    this._display = false;
    this._timeoutHandler = setTimeout(() => {
      this._imageUrl = undefined;
      this._content = undefined;
    }, 300);
  }

}
