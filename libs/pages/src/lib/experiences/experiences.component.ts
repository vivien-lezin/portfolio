import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  FadeInAnimationDirective,
  FadeInFromLeftAnimationDirective,
  FadeInFromRightAnimationDirective,
  FadeInFromTinyAnimationDirective,
  NgIfAnimatedDirective
} from "@portfolio/animation-angular";
import {BigTitleComponent, TimelineButtonComponent} from "@portfolio/features";
import {MatIcon} from "@angular/material/icon";
import {LayoutService} from "@portfolio/layout";
import * as gsap from "gsap";
import {lastValueFrom} from "rxjs";
import * as mammoth from "mammoth";
import {HttpClient} from "@angular/common/http";
import {Meta, Title} from "@angular/platform-browser";

type DateExperience = {
  month: string;
  year: string;
}

type Experience = {
  srcLogo: string;
  title: string;
  post: string;
  from: DateExperience;
  to: DateExperience;
  description: string;
  kind?: string;
  url?: string;
  filePath: string;
}

@Component({
  selector: 'ptf-experiences',
  standalone: true,
  imports: [CommonModule, FadeInAnimationDirective, BigTitleComponent, TimelineButtonComponent, MatIcon, FadeInFromLeftAnimationDirective, NgIfAnimatedDirective, FadeInFromTinyAnimationDirective, FadeInFromRightAnimationDirective],
  templateUrl: './experiences.component.html',
  styleUrl: './experiences.component.scss',
})
export class ExperiencesComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("trait")
  private _traitElement!: ElementRef;

  constructor(
    private readonly _titleService: Title,
    private readonly _metaService: Meta,
    private readonly _httpClient: HttpClient,
    private readonly _deviceService: LayoutService.DeviceService,
    private readonly _articleService: LayoutService.ContentArticleService) {
  }

  private _displayArticle = false;

  public get displayArticle(): boolean {
    return this._displayArticle;
  }

  private _experiences: Experience[] = [
    {
      srcLogo: "./assets/images/esiea-logo.png",
      title: "ESIEA - DAX",
      post: "Mastère expert développement d’application",
      to: {
        month: "AOU.",
        year: "2025",
      },
      from: {
        month: "SEP.",
        year: "2020",
      },
      url: "https://www.esiea.fr/",
      filePath: './assets/documents/Expérience - esiea.docx',
      description: ""
    },
    {
      srcLogo: "./assets/images/thales-logo.png",
      title: "THALES AVS - Osny",
      post: "Développeur WEB",
      to: {
        month: "SEP.",
        year: "2025",
      },
      from: {
        month: "SEP.",
        year: "2023",
      },
      url: "https://www.thalesgroup.com/fr",
      kind: "Alternance",
      filePath: './assets/documents/Expérience - Thales Osny.docx',
      description: ""
    },
    {
      srcLogo: "./assets/images/thales-logo.png",
      title: "THALES SIX - LAVAL",
      post: "Développeur WEB",
      from: {
        month: "FEV.",
        year: "2023",
      },
      to: {
        month: "SEP.",
        year: "2023",
      },
      url: "https://www.thalesgroup.com/fr",
      kind: "Stage",
      filePath: './assets/documents/Expérience - Thales Laval.docx',
      description: ""
    },
    {
      srcLogo: "./assets/images/formasup-logo.png",
      title: "FORMASUP - Montauban",
      post: "Titre professionnel Web designer",
      from: {
        month: "SEP.",
        year: "2019",
      },
      to: {
        month: "JUI.",
        year: "2020",
      },
      url: "https://formasup.org/",
      filePath: './assets/documents/Expérience - Titre professionnel web designer.docx',
      description: ""
    },
    {
      srcLogo: "./assets/images/3r-logo.png",
      title: "Recherche et réalisation Rémi - Montauban",
      post: "Technicien polyvalent",
      from: {
        month: "OCT.",
        year: "2013",
      },
      to: {
        month: "JAN.",
        year: "2019",
      },
      kind: "CDI",
      url: "https://3r-labo.com/",
      filePath: './assets/documents/Expérience - 3R.docx',
      description: ""
    }];

  public get experiences(): Experience[] {
    return this._experiences;
  }

  private _experienceToDisplay?: Experience;

  public get experienceToDisplay(): Experience | undefined {
    return this._experienceToDisplay;
  }

  public get durationEachElement(): number {
    return 0.3;
  }

  public async ngOnInit(): Promise<void> {
    this._titleService.setTitle("Expériences | Vivien LEZIN - Développeur web fullstack");
    this._metaService.updateTag({
      name: 'description',
      content: 'Parcourez mon expérience professionnelle atypique, mes projets passés et mes collaborations. Expertise prouvée en développement web et en innovation.'
    });
    for (const experience of this._experiences) {
      const content = lastValueFrom(this._httpClient.get(experience.filePath, {responseType: 'arraybuffer'}));
      experience.description = (await mammoth.convertToHtml({arrayBuffer: await content})).value;
    }
  }

  public timelineClicked(experience?: Experience) {
    const checkDevice = this._deviceService.isLargeScreen;
    if (experience) {
      if (!checkDevice) {
        this._articleService.setContent(
          (experience.kind ? `<p><em>${experience.kind}</em></p>` : "") +
          `
        ${experience.description}
        `, experience.title, experience.srcLogo);
      } else {
        this._displayArticle = true;
        this._experienceToDisplay = experience;
      }
    } else {
      if (!checkDevice) {
        this._articleService.removeContent();
      }
      this._displayArticle = false;
      setTimeout(() => {
        this._experienceToDisplay = undefined;
      }, this.durationEachElement * 1000);
    }
  }

  public ngAfterViewInit(): void {
    const timeline = gsap.gsap.timeline();
    timeline.fromTo(this._traitElement.nativeElement, {
      height: "0",
      paused: true,
      reversed: true,
      ease: "none",
    }, {
      duration: this._experiences.length * this.durationEachElement,
      height: "calc(400%)"
    });

    timeline.play();

  }

  public ngOnDestroy() {
    this._articleService.removeContent();
  }
}
