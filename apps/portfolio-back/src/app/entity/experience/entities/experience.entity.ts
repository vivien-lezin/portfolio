import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Skill } from '../../skill/entities/skill.entity';

@Entity('experience')
export class Experience {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  start: Date;

  @Column()
  end: Date;

  @Column()
  subtitle: string;

  @Column({
    type: 'text',
  })
  presentation: string;

  @Column({
    name: 'is_in_company',
  })
  isInCompany: boolean;

  @Column({
    name: 'company_name',
  })
  companyName: string;

  @Column({
    name: 'url_company_photo',
  })
  urlCompanyPhoto: string;

  @Column({
    name: 'url_company',
  })
  urlCompany: string;

  @ManyToMany(() => Skill)
  @JoinTable({
    name:"experience_has_skill",
  })
  skills: Skill[];
}
