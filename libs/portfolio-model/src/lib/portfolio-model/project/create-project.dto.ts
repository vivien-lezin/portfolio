export class CreateProjectDto {
  label: string;

  title: string;

  article: number;

  photo?: string;
}
