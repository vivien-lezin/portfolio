import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BigTitleComponent, DesktopComponent, MobileComponent} from "@portfolio/features";
import {CreationType} from "@portfolio/portfolio-type";
import {LayoutService} from "@portfolio/layout";
import {CreationsService} from "./service";
import {ActivatedRoute} from "@angular/router";
import {lastValueFrom} from "rxjs";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'ptf-creations',
  standalone: true,
  imports: [CommonModule, BigTitleComponent, DesktopComponent, MobileComponent],
  templateUrl: './creations.component.html',
  styleUrl: './creations.component.scss',
})
export class CreationsComponent implements OnInit {

  private _isMobile!: boolean;

  constructor(
    private readonly _titleService: Title,
    private readonly _metaService: Meta,
    private _creationsService: CreationsService,
    private _route: ActivatedRoute,
    private _deviceService: LayoutService.DeviceService) {
  }

  private _isSmallScreen!: boolean;

  public get isSmallScreen(): boolean {
    return this._isSmallScreen;
  }

  public get creations(): CreationType.CreationToDisplay[] {
    return this._creationsService.creationsToDisplay;
  }

  public get isDesktop(): boolean {
    return this._deviceService.isDesktop;
  }

  public ngOnInit(): void {
    this._titleService.setTitle("Réalisations | Vivien LEZIN - Développeur web fullstack");
    this._metaService.updateTag({
      name: 'description',
      content: 'Découvrez mes projets réalisés : sites web dynamiques, applications modernes et solutions innovantes. Conception sur mesure avec le souci du détail.'
    });
    this._route.paramMap.subscribe(async (params) => {
      const selectedId = params.get('id');
      if (!this._creationsService.ready) {
        await lastValueFrom(this._creationsService.ready$);
      }
      if (selectedId) {
        this._creationsService.selectedId = selectedId;
      } else {
        this._creationsService.selectedId = "";
      }
    });
  }
}
