import {Creation} from "./creation.type";

export type CreationToDisplay = {
  display: boolean,
  zIndex: number,
  mainFocus: boolean,
} & Creation;
