import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExperienceModule } from './entity/experience/experience.module';
import { SkillModule } from './entity/skill/skill.module';
import { ProjectModule } from './entity/project/project.module';
import { Experience } from './entity/experience/entities/experience.entity';
import { Project } from './entity/project/entities/project.entity';
import { Skill } from './entity/skill/entities/skill.entity';
import { ConfigModule } from '@nestjs/config';
import config from '../config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      load:[config]
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'portfolio',
      password: 'portfolio',
      database: 'portfolio',
      entities: [Experience, Project, Skill],
      //todo: à enlever en production
      synchronize: true,
      dropSchema: true,
    }),
    ExperienceModule,
    SkillModule,
    ProjectModule,
  ],
})
export class AppModule {}
