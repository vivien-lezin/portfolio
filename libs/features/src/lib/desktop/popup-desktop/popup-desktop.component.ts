import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {CdkDrag, CdkDragHandle} from "@angular/cdk/drag-drop";
import {MatIcon} from "@angular/material/icon";
import {
  FadeInFromTinyAnimationDirective,
  FadeOutAnimationDirective,
  NgIfAnimatedDirective
} from "@portfolio/animation-angular";
import {MatProgressSpinner} from "@angular/material/progress-spinner";

@Component({
  selector: 'ptf-popup-desktop',
  standalone: true,
  imports: [CommonModule, CdkDrag, CdkDragHandle, MatIcon, NgOptimizedImage, FadeInFromTinyAnimationDirective, MatProgressSpinner, FadeOutAnimationDirective, NgIfAnimatedDirective],
  templateUrl: './popup-desktop.component.html',
  styleUrl: './popup-desktop.component.scss',
})
export class PopupDesktopComponent implements OnInit, OnDestroy {
  @ViewChild('popupWindow') popupWindow!: ElementRef;
  @Input({required: true}) public iconImg!: string;
  @Input({required: true}) public title!: string;
  @Input({required: true}) public zIndex!: number;
  @Input({required: true}) public content!: string;
  @Input() public mainImg?: string;
  @Input({required: true}) public mainFocus!: boolean;
  @Output("closed") private _closed = new EventEmitter<string>();
  @Output("focused") private _focused = new EventEmitter<string>();
  @Output("windowGrown") private _windowGrown = new EventEmitter<string>();
  private _isResizing = false;
  private _isDragging = false;
  private _resizeDirection!: string;
  private _lastMouseX!: number;
  private _lastMouseY!: number;
  private _timeoutHandler: number[] = [];

  private _valueSpinner = 0;

  public get valueSpinner() {
    return this._valueSpinner;
  }

  private _loaderEnd = false;

  public get loaderEnd() {
    return this._loaderEnd;
  }

  private _display = true;

  public get display(): boolean {
    return this._display;
  }

  public ngOnInit() {
    this._valueSpinner = 0;
    this._timeoutHandler.push(setTimeout(() => {
      this._valueSpinner = 25;
    }, 200));
    this._timeoutHandler.push(setTimeout(() => {
      this._valueSpinner = 45;
    }, 500));
    this._timeoutHandler.push(setTimeout(() => {
      this._valueSpinner = 100;
    }, 1000));
    this._timeoutHandler.push(setTimeout(() => {
      this._loaderEnd = true;
    }, 1500));
  }

  public onDragStart(event: MouseEvent) {
    if (!this.mainFocus) {
      this._isDragging = true;
      this._beginInteraction(event);
    }
  }

  public onDragEnd() {
    this._isDragging = false;
  }

  public onResizeStart(event: MouseEvent, direction: string) {
    if (!this.mainFocus) {
      this._resizeDirection = direction;
      this._isResizing = true;
      this._beginInteraction(event);
    }
  }

  @HostListener('document:mousemove', ['$event'])
  public onResizeMove(event: MouseEvent) {
    this._onMoveHandler(event);
    this._onResizeHandler(event);
  }

  @HostListener('document:mouseup')
  public onResizeEnd() {
    this._isResizing = false;
  }

  public close() {
    this._display = false;
    setTimeout(() => {
      this._closed.emit(this.title);
    }, 300)
  }

  public growWindow(event: MouseEvent) {
    this._windowGrown.emit(this.title);
    event.preventDefault()
  }

  public ngOnDestroy() {
    this._valueSpinner = 0;
    document.removeEventListener('mousemove', this.onResizeMove);
    document.removeEventListener('mouseup', this.onResizeEnd);
    this._timeoutHandler.forEach((t) => clearTimeout(t));
  }

  public clicked() {
    this._focused.emit(this.title);
  }

  private _onMoveHandler(event: MouseEvent) {
    if (this._isDragging) {
      const offsetX = event.clientX - this._lastMouseX;
      const offsetY = event.clientY - this._lastMouseY;

      const newLeft = this.popupWindow.nativeElement.offsetLeft + offsetX;
      const newTop = this.popupWindow.nativeElement.offsetTop + offsetY;

      this.popupWindow.nativeElement.style.left = `${newLeft}px`;
      this.popupWindow.nativeElement.style.top = `${newTop}px`;

      this._lastMouseX = event.clientX;
      this._lastMouseY = event.clientY;
    }
  }

  private _beginInteraction(event: MouseEvent) {
    this._lastMouseX = event.clientX;
    this._lastMouseY = event.clientY;
    event.preventDefault();
  }

  private _onResizeHandler(event: MouseEvent) {
    if (this._isResizing) {
      const offsetX = event.clientX - this._lastMouseX;
      const offsetY = event.clientY - this._lastMouseY;
      const rect = this.popupWindow.nativeElement.getBoundingClientRect();
      const newLeft = rect.left + offsetX;
      const newTop = rect.top + offsetY;
      switch (this._resizeDirection) {
        case 'right':
          this.popupWindow.nativeElement.style.width = `${rect.width + offsetX}px`;
          break;
        case 'bottom':
          this.popupWindow.nativeElement.style.height = `${rect.height + offsetY}px`;
          break;
        case 'left':
          this.popupWindow.nativeElement.style.width = `${rect.width - offsetX}px`;
          this.popupWindow.nativeElement.style.left = `${rect.left + offsetX}px`;
          break;
        case 'top':
          this.popupWindow.nativeElement.style.height = `${rect.height - offsetY}px`;
          this.popupWindow.nativeElement.style.top = `${newTop}px`;
          break;
        case 'bottom-right':
          this.popupWindow.nativeElement.style.width = `${rect.width + offsetX}px`;
          this.popupWindow.nativeElement.style.height = `${rect.height + offsetY}px`;
          break;
        case 'bottom-left':
          this.popupWindow.nativeElement.style.width = `${rect.width - offsetX}px`;
          this.popupWindow.nativeElement.style.left = `${newLeft}px`;
          this.popupWindow.nativeElement.style.height = `${rect.height + offsetY}px`;
          break;
        case 'top-right':
          this.popupWindow.nativeElement.style.width = `${rect.width + offsetX}px`;
          this.popupWindow.nativeElement.style.height = `${rect.height - offsetY}px`;
          this.popupWindow.nativeElement.style.top = `${newTop}px`;
          break;
        case 'top-left':
          this.popupWindow.nativeElement.style.width = `${rect.width - offsetX}px`;
          this.popupWindow.nativeElement.style.left = `${newLeft}px`;
          this.popupWindow.nativeElement.style.height = `${rect.height - offsetY}px`;
          this.popupWindow.nativeElement.style.top = `${newTop}px`;
          break;
      }

      this._lastMouseX = event.clientX;
      this._lastMouseY = event.clientY;

    }
  }
}
