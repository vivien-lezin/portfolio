import {Injectable} from '@angular/core';
import {CreationType} from "@portfolio/portfolio-type";
import * as mammoth from 'mammoth';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, lastValueFrom, Observable} from "rxjs";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class CreationsService {

  constructor(private readonly _httpClient: HttpClient, private readonly _router: Router) {
    this._init().then(() => {
      this._creations.forEach((creation, index) => {
        this._creationsToDisplay.push({
          ...creation,
          zIndex: 1020 + index + 1,
          display: false,
          mainFocus: false,
        });
      });
      this._ready.next(true);
      this._ready.complete();
    });
  }

  private _ready = new BehaviorSubject(false);

  public get ready(): boolean {
    return this._ready.value;
  }

  public get ready$(): Observable<boolean> {
    return this._ready.asObservable();
  }

  private _creations: CreationType.Creation[] = [
    {//OK
      iconSrc: "assets/documents/icon/Réalisation - annonce.com.png",
      title: 'Annonce.com',
      mainImg: 'assets/documents/couverture/Réalisation - annonce.com.png',
      slug: 'annonce-com',
      filePath: "assets/documents/Réalisation - annonce.com.docx",
      content: ``
    }, { //OK
      iconSrc: "assets/documents/icon/Réalisation - Junior entreprise.png",
      title: 'Junior Entreprise',
      slug: 'junior-entreprise',
      filePath: "assets/documents/Réalisation - Junior entreprise.docx",
      content: ""
    }, { //Ok
      iconSrc: "assets/documents/icon/Réalisation - ok-voiture.png",
      title: 'Ok-voiture',
      slug: 'ok-voiture',
      mainImg: 'assets/documents/couverture/Réalisation - ok-voiture.png',
      filePath: "assets/documents/Réalisation - ok-voiture.docx",
      content: ""
    }, { //Ok
      iconSrc: "assets/documents/icon/Réalisation - Pist'audit.png",
      title: 'Pist\'audit',
      slug: 'pist-audit',
      filePath: "assets/documents/Réalisation - Pist'audit.docx",
      content: ""
    }, { //Ok
      iconSrc: "assets/documents/icon/Réalisation - PPI Web.png",
      title: 'PPI Web',
      slug: 'ppi-web',
      filePath: "assets/documents/Réalisation - PPI Web.docx",
      content: ""
    }
  ]

  get creations(): CreationType.Creation[] {
    return this._creations;
  }

  public set selectedId(id: string) {
    const creationFound = this._creationsToDisplay.find((c) => c.slug === id)
    this._creationsToDisplay.forEach((creation) => {
      creation.display = false;
      creation.mainFocus = false;
    });
    if (creationFound) {
      creationFound.display = true;
      creationFound.mainFocus = true;
    }
  }

  private _creationsToDisplay: CreationType.CreationToDisplay[] = []

  public get creationsToDisplay(): CreationType.CreationToDisplay[] {
    return this._creationsToDisplay;
  }

  private async _init() {
    for (const creation of this._creations) {
      const content = lastValueFrom(this._httpClient.get(creation.filePath, {responseType: 'arraybuffer'}));
      creation.content = (await mammoth.convertToHtml({arrayBuffer: await content})).value;
    }
  }
}
