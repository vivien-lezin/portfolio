import {Component, EventEmitter, Input, input, Output} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ptf-vertical-button',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './vertical-button.component.html',
  styleUrl: './vertical-button.component.scss',
})
export class VerticalButtonComponent {
  public isHard = input.required<boolean>();
  @Output()
  public clickEmitter = new EventEmitter<boolean>();
  @Input()
  public isVertical = true
}
