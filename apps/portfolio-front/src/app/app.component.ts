import {Component} from '@angular/core';
import {NavigationEnd, Router, RouterModule} from '@angular/router';
import {FadeInAnimationDirective, NgIfAnimatedDirective} from "@portfolio/animation-angular";
import {NgIf} from "@angular/common";
import {MatIcon} from "@angular/material/icon";
import {MatButton} from "@angular/material/button";
import {ArticleComponent, HeaderComponent, MenuComponent, LayoutService} from '@portfolio/layout';
import {Link, MenuBurgerComponent, MenuCarouselComponent, MenuCarouselTabletComponent,} from "@portfolio/features";
import {filter} from "rxjs";
import {CreationsComponent, CreationsService, SkillsComponent, SkillsService} from "@portfolio/pages";
import {Meta} from "@angular/platform-browser";

@Component({
  standalone: true,
  imports: [RouterModule, FadeInAnimationDirective, NgIfAnimatedDirective, NgIf, MatIcon, HeaderComponent, MatButton, MenuComponent, MenuCarouselComponent, MenuCarouselTabletComponent, ArticleComponent, MenuBurgerComponent],
  selector: 'portfolio-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  public openMenu = false;
  public links: Array<Link> = [
    {
      path: '',
      enabled: false,
      label: "Présentation"
    },
    {
      path: 'compétences',
      enabled: false,
      label: "Compétences",
      subLink:[]
    },
    {
      path: 'expériences',
      enabled: false,
      label: "Expériences",
    },
    {
      path: 'réalisations',
      enabled: false,
      label: "Réalisations",
      subLink:[]
    },
  ];

  constructor(
    private readonly _router: Router,
    _creationsService: CreationsService,
    _skillsService: SkillsService,
    private _contentArticleService: LayoutService.ContentArticleService,
    private readonly _metaService: Meta) {
    console.log("%cCeci n'est pas un bug, c'est une fonctionnalité 😉", "color: cyan; font-size: 18px;");
    console.log("%cSi tu es là pour chercher des bugs, il n'y en a pas 😜", "color: cyan; font-size: 14px;");

    this._metaService.addTag({
      name: 'description',
      content: ''
    });
    this._router.events.pipe(
      filter((value) => value instanceof NavigationEnd)
    ).subscribe((value) => {
      this.links.forEach((l) => {
        l.enabled = false;
      });
      const str = decodeURI((value as NavigationEnd).url.substring(1));
      const link = this.links.find((l) => l.path === str.split("/")[0]);
      if (link) link.enabled = true;
      this.openMenu = false;
    });
    const creationsLink = this.links.find((link) => link.label === "Réalisations");
    if (creationsLink) {
      _creationsService.creations.forEach((creation) => {
        creationsLink.subLink?.push({
          label: creation.title,
          path: `${creationsLink.path}/${creation.slug}`,
          component: CreationsComponent,
        });
      });
    }
    const skillsLink = this.links.find((link) => link.label === "Compétences");
    if (skillsLink) {
      _skillsService.allSkills.forEach((skill) => {
        skillsLink.subLink?.push({
          label: skill.name,
          path: `${skillsLink.path}/${skill.slug}`,
          component: SkillsComponent
        });
      });
    }
  }

  public get displayArticle(): boolean {
    return this._contentArticleService.display;
  }

  public buttonClicked(link: Link) {
    this._router.navigate([link.path]);
  }
}
