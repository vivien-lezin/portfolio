import {Route} from "@angular/router";

export type Link = Route & {
  label: string;
  enabled?: boolean;
  subLink?: Array<Link>;
};
