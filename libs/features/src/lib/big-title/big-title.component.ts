import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import SplitType from 'split-type';
import {gsap} from "gsap";

@Component({
  selector: 'ptf-big-title',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './big-title.component.html',
  styleUrl: './big-title.component.scss',
})
export class BigTitleComponent implements AfterViewInit{


  @ViewChild("title") private _titleElt!: ElementRef<HTMLTitleElement>;

  public ngAfterViewInit(): void {
    const chars = SplitType.create(this._titleElt.nativeElement).chars;

    gsap.from(chars,{
      stagger:0.05,
      duration: 1,
      y: 175,
      ease:"power4.out"
    });
  }


}
