export class CreateSkillDto {
  label: string;

  title: string;

  description: string;

  level: number;

  isTechnical: boolean;

  article: number;

  photo: string;

  externalUrl?: string;
}
