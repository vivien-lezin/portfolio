import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ptf-timeline-button',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './timeline-button.component.html',
  styleUrl: './timeline-button.component.scss',
})
export class TimelineButtonComponent {

  @Input() active?: boolean;

}
