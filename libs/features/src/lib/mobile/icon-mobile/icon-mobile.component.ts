import {Component, EventEmitter, Input, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatIcon} from "@angular/material/icon";

@Component({
  selector: 'ptf-icon-mobile',
  standalone: true,
  imports: [CommonModule, MatIcon],
  templateUrl: './icon-mobile.component.html',
  styleUrl: './icon-mobile.component.scss',
})
export class IconMobileComponent {

  @Input() iconSrc!: string;
  @Input() iconText!: string;
  @Output() touchEnd = new EventEmitter<string>();

}
