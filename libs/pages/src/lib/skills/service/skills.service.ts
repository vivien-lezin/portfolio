import {Injectable} from '@angular/core';
import {SkillType} from "@portfolio/portfolio-type";
import {BehaviorSubject, lastValueFrom, Observable} from "rxjs";
import * as mammoth from "mammoth";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SkillsService {

  private readonly _skills: SkillType.Skill[] = [
    { //ok
      name: 'Node.js',
      tokenImg: './assets/documents/icon/token_node.png',
      content: "",
      isHard: true,
      slug: "node-js",
      note: 3,
      filePath: './assets/documents/Compétence - Node-js.docx'
    },
    { //ok
      name: 'Angular',
      tokenImg: './assets/documents/icon/token_angular.png',
      content: "",
      isHard: true,
      slug: "angular",
      note: 5,
      filePath: './assets/documents/Compétence - Angular.docx'
    },
    {
      name: 'Conception d\'une base de données',
      tokenImg: './assets/documents/icon/token_bdd.png',
      content: "",
      isHard: true,
      slug: "base-de-donnees",
      note: 2,
      filePath: './assets/documents/Compétence - Conception de base de données.docx'
    }, { //Ok
      name: 'Conception d\'une API',
      tokenImg: './assets/documents/icon/token_api.png',
      content: "",
      isHard: true,
      slug: "api",
      note: 2,
      filePath: './assets/documents/Compétence - Conception d\'une API REST.docx'
    }, { //ok
      name: 'Architecture logicielle',
      tokenImg: './assets/documents/icon/token_archi.png',
      content: "",
      isHard: true,
      slug: "architecture-logicielle",
      note: 2,
      filePath: './assets/documents/Compétence - Architecture logicielle.docx'
    }, { //Ok
      name: 'UX/UI design',
      tokenImg: './assets/documents/icon/token_ux-ui.png',
      content: "",
      isHard: true,
      slug: "ux-ui-design",
      note: 5,
      filePath: './assets/documents/Compétence - Conception d\'interface graphique.docx'
    }, { //Ok
      name: 'Adaptabilité',
      tokenImg: './assets/documents/icon/token_adaptabilite.png',
      content: "",
      isHard: false,
      slug: "adaptabilité",
      note: 5,
      filePath: './assets/documents/Compétence - Adaptabilité.docx'
    }, { //Ok
      name: 'Résolution de problèmes',
      tokenImg: './assets/documents/icon/token_resolution-de-problemes.png',
      content: "",
      isHard: false,
      slug: "résolution-de-problèmes",
      note: 5,
      filePath: './assets/documents/Compétence - Résolution de problèmes.docx'
    }, { //Ok
      name: 'Autonomie',
      tokenImg: './assets/documents/icon/token_autonomie.png',
      content: "",
      isHard: false,
      slug: "autonomie",
      note: 5,
      filePath: './assets/documents/Compétence - autonomie.docx'
    }, { //Ok
      name: 'Gestion de projet',
      tokenImg: './assets/documents/icon/token_gestion-de-projet.png',
      content: "",
      isHard: false,
      slug: "gestion-de-projet",
      note: 5,
      filePath: './assets/documents/Compétence - Gestion de projet.docx'
    },
  ];

  constructor(private readonly _httpClient: HttpClient) {
    this._init().then(() => {
      this._ready.next(true);
      this._ready.complete();
      this._defineSkillsToDisplay(this._isHard.value);
      this._isHard.subscribe((isHard) => {
        this._defineSkillsToDisplay(isHard);
      });
    })
  }

  private _ready: BehaviorSubject<boolean> = new BehaviorSubject(false);

  public get ready(): boolean {
    return this._ready.value;
  }

  public get ready$(): Observable<boolean> {
    return this._ready.asObservable();
  }

  private _skillsToDisplay!: SkillType.SkillToDisplay[];

  public get skillsToDisplay(): SkillType.SkillToDisplay[] {
    return this._skillsToDisplay;
  }

  private _isHard: BehaviorSubject<boolean> = new BehaviorSubject(true);

  public get isHard(): boolean {
    return this._isHard.value;
  }

  public set isHard(isHard: boolean) {
    this._isHard.next(isHard);
  }

  public get allSkills(): SkillType.Skill[] {
    return this._skills;
  }

  private async _init() {
    for (const skill of this._skills) {
      const content = lastValueFrom(this._httpClient.get(skill.filePath, {responseType: 'arraybuffer'}));
      skill.content = (await mammoth.convertToHtml({arrayBuffer: await content})).value;
    }
  }

  private _defineSkillsToDisplay(isHard: boolean): void {
    let index = 0
    this._skillsToDisplay = this._skills
      .filter((skill) => skill.isHard === isHard)
      .map((skill: SkillType.Skill) => {
        index++;
        return {
          ...skill,
          highlight: 10 + index
        } as SkillType.SkillToDisplay;
      });
  }
}
