import {Skill} from "./skill.type";

export type SkillToDisplay = {
  highlight: number;
} & Skill
