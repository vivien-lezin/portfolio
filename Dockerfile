# Étape 1 : Construire l'image de base à partir de Node.js
FROM node:18-alpine as build

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier le fichier package.json et package-lock.json (si présent) pour installer les dépendances
COPY package.json package-lock.json ./

# Installer les dépendances de l'application
RUN npm install -g nx@latest
RUN npm install

# Copier tout le code source de l'application dans le conteneur
COPY . .

# Construire l'application avec Nx
RUN nx build portfolio-front --prod

# Vérification de l'existence du répertoire de build
RUN ls -alh /app/dist

# Étape 2 : Créer l'image pour la production (serveur léger)
FROM nginx:alpine

# Créer la configuration Nginx via des échos dans le fichier de configuration
RUN echo "server {" > /etc/nginx/conf.d/default.conf && \
    echo "    listen 80;" >> /etc/nginx/conf.d/default.conf && \
    echo "    server_name _;" >> /etc/nginx/conf.d/default.conf && \
    echo "    root /usr/share/nginx/html;" >> /etc/nginx/conf.d/default.conf && \
    echo "    index index.html;" >> /etc/nginx/conf.d/default.conf && \
    echo "" >> /etc/nginx/conf.d/default.conf && \
    echo "    location / {" >> /etc/nginx/conf.d/default.conf && \
    echo "        try_files \$uri \$uri/ /index.html;" >> /etc/nginx/conf.d/default.conf && \
    echo "    }" >> /etc/nginx/conf.d/default.conf && \
    echo "}" >> /etc/nginx/conf.d/default.conf

# Copier les fichiers de l'application construite depuis l'étape précédente
COPY --from=build /app/dist/apps/portfolio-front /usr/share/nginx/html

# Exposer le port 80 pour accéder à l'application
EXPOSE 80

# Démarrer Nginx pour servir l'application
CMD ["nginx", "-g", "daemon off;"]
