import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Experience } from '../../experience/entities/experience.entity';
import { Project } from '../../project/entities/project.entity';

@Entity('skill')
export class Skill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  level: number;

  @Column({
    name: 'is_technical',
  })
  isTechnical: boolean;

  @Column({
    type: 'text',
  })
  article: number;

  @Column({
    nullable: true,
  })
  photo: string;

  @Column({
    nullable: true,
    name: 'external_url',
  })
  externalUrl?: string;

  @ManyToMany(() => Experience)
  @JoinTable({
    name:"skill_has_experience",
  })
  experiences: Experience[];

  @ManyToMany(() => Project)
  @JoinTable({
    name:"skill_has_project",
  })
  projects: Project[];
}
