import {CoreAnimationDirective} from "./core-animation-directive";
import {AfterViewInit, Directive, ElementRef} from "@angular/core";

@Directive({
  selector: '[ptfHeightOutAnimation]',
  standalone: true,
})
export class HeightOutAnimationDirective extends CoreAnimationDirective implements AfterViewInit{
  constructor(protected override element: ElementRef) {
    super(element);
  }
  public override ngAfterViewInit(): void {
    super.ngAfterViewInit()
    // perform animation
    this._animateIn();
  }
  protected override _animateIn() {
    this._timeline.from(this.element.nativeElement, {
      duration: this.duration,
      height:0,
      delay: this.delay
    })
    super._animateIn();
  }
}
