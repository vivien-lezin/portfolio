import {Component, EventEmitter, input, Output} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {Link} from "./type/menu-carousel.type";
import {ButtonCarouselComponent} from "./button-carousel/button-carousel.component";
import {
  FadeInAnimationDirective,
  FadeInFromBottomAnimationDirective,
  NgIfAnimatedDirective
} from "@portfolio/animation-angular";
import {RouterLink, RouterLinkActive} from "@angular/router";

@Component({
  selector: 'ptf-menu-carousel',
  standalone: true,
  imports: [CommonModule, ButtonCarouselComponent, NgOptimizedImage, NgIfAnimatedDirective, FadeInAnimationDirective, FadeInFromBottomAnimationDirective,  RouterLink, RouterLinkActive],
  templateUrl: './menu-carousel.component.html',
  styleUrl: './menu-carousel.component.scss',
})
export class MenuCarouselComponent {
  public links = input.required<Array<Link>>();
  public label?: string;
  @Output()
  public buttonClicked: EventEmitter<Link> = new EventEmitter<Link>();

  public onEnterButton(label: string): void {
    this.label = label;
  }

  public onOutButton(): void {
    this.label = undefined;
  }

  onClickButtonCarrousel(link: Link) {
    this.buttonClicked.emit(link);
  }
}
