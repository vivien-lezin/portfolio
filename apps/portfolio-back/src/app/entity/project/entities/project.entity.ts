import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Skill } from '../../skill/entities/skill.entity';

@Entity('project')
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column()
  title: string;

  @Column({
    type: 'text',
  })
  article: number;

  @Column({
    nullable: true,
  })
  photo?: string;

  @ManyToMany(() => Skill)
  @JoinTable({
    name:"project_has_skill",
  })
  skills: Skill[];
}
