import {
  AfterViewInit, Component, effect, ElementRef, EventEmitter, input, Input, Output,
  ViewChild
} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FadeInFromLeftAnimationDirective} from "@portfolio/animation-angular";
import {MatIcon} from "@angular/material/icon";
import {gsap} from "gsap";
import {Link} from "@portfolio/features";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'ptf-linkMenu',
  standalone: true,
  imports: [CommonModule, FadeInFromLeftAnimationDirective, MatIcon, RouterLink],
  templateUrl: './link-menu.component.html',
  styleUrl: './link-menu.component.scss',
})
export class LinkMenuComponent implements AfterViewInit {

  @Input({required:true}) public mainLink!: string;
  @Input() public childrenLink?: Array<Link>;
  public isExpanded = input(true);
  @Output() public expanded: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild("subMenu") private _subMenuElt?: ElementRef;

  private _timeline?: gsap.core.Timeline;

  constructor() {
    effect(() => {
      if (this.isExpanded()){
        this._timeline?.play();
      } else {
        this._timeline?.reverse();
      }
    });
  }

  public ngAfterViewInit(): void {
    if (this._subMenuElt){
      this._timeline=gsap.timeline({
        paused:true,
        reversed:true
      })
      this._timeline.from(this._subMenuElt.nativeElement, {
        duration: 0.3,
        height: 0,
        delay:0,
        ease:'linear'
      });
    }
  }

  public onClickExpand(){
    this.expanded.emit(this.isExpanded());
  }
}
