import {AfterViewInit, Directive, ElementRef} from '@angular/core';
import {CoreAnimationDirective} from "./core-animation-directive";

@Directive({
  selector: '[ptfFadeOutAnimation]',
  standalone: true,
})
export class FadeOutAnimationDirective extends CoreAnimationDirective implements AfterViewInit {
  constructor(protected override element: ElementRef) {
    super(element);
  }

  public override ngAfterViewInit(): void {
    super.ngAfterViewInit()
    // perform animation
    this._animateIn();
  }

  protected override _animateIn() {
    this._timeline.to(this.element.nativeElement, {
      duration: this.duration,
      opacity: 0,
      display: 'none',
      delay: this.delay
    });
    super._animateIn();
  }
}
