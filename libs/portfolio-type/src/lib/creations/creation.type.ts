export type Creation = {
  iconSrc: string,
  title: string,
  mainImg?: string,
  content: string,
  slug: string,
  filePath: string,
}
