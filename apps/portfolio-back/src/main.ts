/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';

import generate from './fixture/fixtures';
import config from './config/configuration';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  await app.listen(config().port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${config().port}/${globalPrefix}`
  );
}

bootstrap().then(()=>{
  generate();
});
