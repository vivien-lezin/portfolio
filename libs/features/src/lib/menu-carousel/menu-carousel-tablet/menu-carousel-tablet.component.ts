import {Component, EventEmitter, input, OnDestroy, Output} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonCarouselComponent} from "../button-carousel/button-carousel.component";
import {FadeInFromBottomAnimationDirective} from "@portfolio/animation-angular";
import {Link} from "../type/menu-carousel.type";

@Component({
  selector: 'ptf-menu-carousel-tablet',
  standalone: true,
  imports: [CommonModule, ButtonCarouselComponent, FadeInFromBottomAnimationDirective],
  templateUrl: './menu-carousel-tablet.component.html',
  styleUrl: './menu-carousel-tablet.component.scss',
})
export class MenuCarouselTabletComponent implements OnDestroy {
  label?: string;
  links = input.required<Link[]>();
  @Output()
  public buttonClicked: EventEmitter<Link> = new EventEmitter<Link>();
  private _timeouthandler?: number;

  onEnterButton(label: string) {
    this.label = label;
    clearTimeout(this._timeouthandler);
    this._timeouthandler = setTimeout(() => {
      this.label = undefined;
    }, 3000);
  }

  onOutButton(): void {
    this.label = undefined
  }

  onClickButtonCarrousel(link: Link) {
    this.buttonClicked.emit(link);
  }

  public ngOnDestroy(): void {
    clearTimeout(this._timeouthandler);
  }

}
