export class CreateExperienceDto {
  label: string;

  start: Date;

  end: Date;

  subtitle: string;

  presentation: string;

  isInCompany: boolean;

  companyName: string;

  urlCompanyPhoto: string;

  urlCompany: string;
}
