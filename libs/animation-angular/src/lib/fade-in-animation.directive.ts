import {AfterViewInit, Directive, ElementRef} from '@angular/core';
import {CoreAnimationDirective} from "./core-animation-directive";

@Directive({
  selector: '[ptfFadeIn]',
  standalone: true,
})
export class FadeInAnimationDirective extends CoreAnimationDirective implements AfterViewInit {
  constructor(protected override element: ElementRef) {
    super(element);
  }

  public override ngAfterViewInit(): void {
    super.ngAfterViewInit()
    // perform animation
    this._animateIn();
  }

  protected override _animateIn() {
    this._timeline.from(this.element.nativeElement, {
      duration: this.duration,
      opacity:0,
      delay: this.delay
    })
    super._animateIn();
  }
}
